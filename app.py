from flask import Flask, url_for, request, render_template, jsonify
from imutils import paths
from pyimagesearch.panorama import Stitcher
import numpy as np
import argparse
import imutils
import cv2
import json
import os

app = Flask(__name__)

@app.route('/', methods=['POST'])
def process_stitch():
    if request.method == "POST":
        data = request.json
        file_paths = data["file"]
        output_name = data["output"]
        images = []
        print(file_paths)
        for file_path in file_paths:
                image = cv2.imread(file_path)
                image = imutils.resize(image, width=400)
                images.append(image)
        stitcher = Stitcher()
        try:
                stitched = stitcher.stitch_multi(images)
        except Exception as e:
                print(e)
                resp = jsonify({"status" : False})
                resp.status_code = 502
                return resp
        cv2.imwrite("static/results/" + output_name, stitched)
        resp = jsonify({"status" : True})
        resp.status_code = 200
        return resp

@app.route('/', methods=['GET'])
def form_stitch():
        files = os.listdir("static/uploads")
        return render_template("form.html", files=files)

@app.route('/images', methods=['GET'])
def get_images_list():
        files = os.listdir("static/uploads")
        resp = jsonify({
                "status":True,
                "data":files
        })
        resp.status_code = 200
        return resp

if __name__ == '__main__':
    app.run()